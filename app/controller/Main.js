Ext.define('Karte.controller.Main', {
    extend: 'Ext.app.Controller',
	init: function(){
		var me = this;
		this.window = Ext.create('Karte.view.Main').show();
		this.control({
			'button[name=printTicketsButton]': {
				click: function(){
					me.preparePrintData()
				}
			},
			'checkbox[name=isTaxable]': {
					change: function(combo,newValue){
						if(newValue == true){
							me.window.query('*[name=taxRate]')[0].enable();
						}else{
							me.window.query('*[name=taxRate]')[0].disable();
						}
					}
			}
		});
		
		this.row = ['<tr style="margin-top: 10px">',
            '<td style="width: 5cm; border: 2px dashed #CCCCCC">',
                '<div style="position: relative; width 5cm; height: 5cm;">',
                    '<div style="position: absolute; top: 10px; left: 10px;">id za DDV: {taxNumber}',
                        '<br />{companyName}',
                        '<br />{address}',
                        '<br />{postNameNumber}</div>',
                    '<div style="position: absolute; top: 2cm; left: 10px;">cena: {price}&euro;',
						'<tpl if="values.isTaxable">',
                        '<br />osnova: {taxBase}&euro;',
                        '<br />DDV ({taxRate}%): {tax}&euro;',
                        '</tpl>',
                        '<tpl if="!values.isTaxable">',
                        '<br />DDV ni obračunan v skladu s 1. odstavkom 94. člena Zakona o DDV',
                        '</tpl>',                        
                        '<br />datum opravljene storitve: {eventDate}</div>',
                    '<div style="border: 1px solid; width: 2cm height: 2cm; font-size: 20px; text-align: center; position: absolute; top: 4cm; left: 10px; right: 10px;">{numberPrefix}{sequenceNumber}</div>',
                '</div>',
            '</td>',
            '<td style="width: 11cm;border-top: 2px dashed #CCCCCC;border-bottom: 2px dashed #CCCCCC;">',
                '<div style="position: relative; width: 11cm; height: 5cm;">',
                    '<div style="z-index: -1; font-size: 50px; position: absolute; width: 100%; top: 3.5cm; left: 10px; color: #cccccc; font-weight: bold;">{price}&euro;</div>',
                    '<div style="z-index: 10; font-size: 40px; position: absolute; top: 10px; left: 10px; text-align: left; width: 6cm; text-align: center;">{productName}</div>',
                    '<div style="width: 5cm; position: absolute; left: 6cm; text-align: right;">',
                        '<div style="position: relative; width: 5cm; height: 5cm;">',
                            '<div style="position: absolute; top: 10px; right: 10px;">id za DDV: {taxNumber}',
                                '<br />{companyName}',
                                '<br />{address}',
                                '<br />{postNameNumber}</div>',
                            '<div style="position: absolute; top: 2cm; right: 10px;">cena: {price}&euro;',
								'<tpl if="values.isTaxable">',
									'<br />osnova: {taxBase}&euro;',
									'<br />DDV ({taxRate}%): {tax}&euro;',	
								'</tpl>',
								'<tpl if="!values.isTaxable">',
									'<br />DDV ni obračunan v skladu s 1. odstavkom 94. člena Zakona o DDV',
								'</tpl>',
                                '<br />datum opravljene storitve: {eventDate}</div>',
                            '<div style="border: 1px solid; width: 2cm height: 2cm; width: 4.2cm; font-size: 20px; text-align: center; position: absolute; top: 4cm; right: 10px;">{numberPrefix}{sequenceNumber}</div>',
                        '</div>',
                    '</div>',
                '</div>',
            '</td>',
            '<td style="width: 5cm; border: 2px dashed #CCCCCC">',
                '<div style="position: relative: width 5cm; height: 5cm;">',
                    '<div style="position: relative; width 5cm; height: 5cm;">',
                        '<div style="position: absolute; top: 10px; left: 10px;">id za DDV: {taxNumber}',
                            '<br />{companyName}',
                            '<br />{address}',
                            '<br />{postNameNumber}</div>',
                        '<div style="position: absolute; top: 2cm;left: 10px;">cena: {price}&euro;',
								'<tpl if="values.isTaxable">',
									'<br />osnova: {taxBase}&euro;',
									'<br />DDV ({taxRate}%): {tax}&euro;',	
								'</tpl>',
								'<tpl if="!values.isTaxable">',
									'<br />DDV ni obračunan v skladu s 1. odstavkom 94. člena Zakona o DDV',
								'</tpl>',
                            '<br />datum opravljene storitve: {eventDate}</div>',
                        '<div style="border: 1px solid; width: 2cm height: 2cm; font-size: 20px; text-align: center; position: absolute; top: 4cm; left: 10px; right: 10px;">{numberPrefix}{sequenceNumber}</div>',
						'<div style="z-index: -1; font-size: 50px; position: absolute; width: 100%; top: 3.5cm; left: 5px; right: 5px; text-align: center; color: #cccccc;font-weight: bold;">{price}&euro;</div>',
                    '</div>',
            '</td>',
        '</tr>']
        this.tableStart = ['<table style="width: 21cm; font-family: Verdana;font-size: 9px;page-break-after:always">', 
						'<tbody>']
		this.tableEnd = ['</tbody>','</table>']

		
	},
	preparePrintData: function(){
		var form = this.window.query('form')[0], data = {}, array = [], pages = [], pageNumber = 0;
		if(!form.isValid()){
				return false;
		}
		 data = form.getForm().getValues();
		 data.taxBase = Ext.Number.toFixed(data.price - (data.price*(data.taxRate/100)),2)
		 data.tax = Ext.Number.toFixed((data.price*(data.taxRate/100)),2)
		 data.priceWithTax = data.taxBase*1 + data.tax*1
		 data.startNumber = data.startNumber*1;
		 data.numberOfTickets = data.numberOfTickets*1
		 for(var i = 0; i < data.numberOfTickets; i++,data.startNumber++){
			if(i%5 == 0){
				pageNumber = pageNumber+1;
				if(i==0){
					pageNumber = 0
				}
			}
			if(!pages[pageNumber]){
				pages[pageNumber] = {
					items: []
				}
			}
			var d = Ext.clone(data);
				d.sequenceNumber = data.startNumber;
				pages[pageNumber].items.push(d)
		}
		var tpl = Ext.create('Ext.XTemplate', '<tpl for=".">',this.tableStart.join(''),'<tpl for="values.items">',this.row.join(''),'</tpl>',this.tableEnd.join(''), '</tpl>').apply(pages);
		Karte.table = tpl;
		window.open('print.html');
	}
	
});
