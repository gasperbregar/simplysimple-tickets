Ext.define("Karte.view.Main", {
        extend: 'Ext.Window',
        title: 'SimplySimple  Tickets',
        minWidth: 600,
        closable: false,
        resizable: false,
        bodyStyle: 'padding: 5px',
        draggable: false,
        layout: 'anchor',
        defaults: {
            labelAlign: 'top',
            labelSeparator: '',
            anchor: '100%'
        },
        tools: [{
                type: 'help',
                tooltip: 'Pomoč',
                handler: function () {
                    Ext.Msg.show({
                            title: 'Mini help',
                            msg: 'Trenutno dela samo v brskalniku Google Chrome <br /> Pri tiskanju izberi Save as PDF, Layout: Portrait, Margins: Minimum, Options: kljukice pri Headers and footers ne sme biti, pri Background colors and images jo pa potrebuješ.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.QUESTION
                        });
                }

            }
        ],
        items: [{
                xtype: 'form',
                border: false,
                defaults: {
                    labelAlign: 'top',
                    labelSeparator: '',
                    anchor: '100%'
                },
                items: [{
                        xtype: 'fieldset',
                        title: 'Podatki o ponudniku',
                        border: true,
                        layout: 'anchor',
                        collapsible: true,
                        defaults: {
                            labelAlign: 'top',
                            labelSeparator: '',
                            anchor: '100%'
                        },
                        items: [{
                                xtype: 'textfield',
                                name: 'taxNumber',
                                fieldLabel: 'Davčna številka',
                                allowBlank: false
                            }, {
                                xtype: 'textfield',
                                name: 'companyName',
                                fieldLabel: 'Naziv podjetja',
                                allowBlank: false,
                            }, {
                                xtype: 'textfield',
                                name: 'address',
                                fieldLabel: 'Naslov',
                                allowBlank: false
                            }, {
                                xtype: 'textfield',
                                name: 'postNameNumber',
                                fieldLabel: 'Pošta in poštna številka',
                                allowBlank: false
                            }
                        ]
                    }, {
                        xtype: 'textfield',
                        name: 'productName',
                        fieldLabel: 'Naziv dogodka'
                    }, {
                        xtype: 'numberfield',
                        name: 'price',
                        fieldLabel: 'Cena [€]',
                        minValue: 0
                    }, {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        anchor: '100%',
                        defaults: {
                            labelAlign: 'top',
                            labelSeparator: ''
                        },
                        items: [{
                                xtype: 'checkbox',
                                boxLabel: 'Zavezanec za DDV',
                                name: 'isTaxable',
                                width: 150,
                                style: 'margin-top: 21px',
                                fieldLabel: ' '
                            }, {
                                xtype: 'combo',
                                name: 'taxRate',
                                fieldLabel: 'DDV',
                                displayField: 'name',
                                valueField: 'id',
                                triggerAction: 'all',
                                queryMode: 'local',
                                value: '22',
                                flex: 1,
                                disabled: true,
                                store: Ext.create('Ext.data.Store', {
                                        fields: ['name', 'id'],
                                        data: [{
                                                name: '9.5%',
                                                'id': '9.5'
                                            }, {
                                                name: '22%',
                                                'id': '22'
                                            }
                                        ],
                                        proxy: {
                                            type: 'memory'
                                        }
                                    })
                            }
                        ]
                    }, {
                        xtype: 'datefield',
                        name: 'eventDate',
                        fieldLabel: 'Datum',
                        format: 'd.m.Y'
                    }, {
                        xtype: 'fieldset',
                        border: true,
                        title: 'Nastavitve tiskanja',
                        layout: 'anchor',
                        defaults: {
                            labelAlign: 'top',
                            labelSeparator: '',
                            anchor: '100%',
                            minValue: 1,
                            allowBlank: false
                        },
                        items: [{
                                xtype: 'numberfield',
                                name: 'numberOfTickets',
                                fieldLabel: 'Koliko kart želiš?',
                                allowBlank: false
                            }, {
                                xtype: 'fieldcontainer',
                                layout: 'hbox',
                                anchor: '100%',
                                defaults: {
                                    labelAlign: 'top',
                                    labelSeparator: ''
                                },
                                items: [{
                                        xtype: 'textfield',
                                        name: 'numberPrefix',
                                        fieldLabel: 'Predpona številke',
                                        tooltip: 'Če želiš imeti pred številko letnico ali kaj podobnega - potem bi številka izgledala 2013-številka ali 2013-123-nekaj'
                                    }, {
                                        xtype: 'numberfield',
                                        name: 'startNumber',
                                        fieldLabel: 'Začetna številka',
                                        flex: 2,
                                        minValue: 0,
                                        allowBlank: false
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ],
        fbar: [{
                xtype: 'button',
                text: 'Natisni',
                name: 'printTicketsButton'
            }
        ]
    });
