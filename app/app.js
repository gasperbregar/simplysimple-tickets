/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides

Ext.application({
    name: 'Karte',
    requires: ['Ext.form.field.Date', 'Ext.form.field.Text', 'Ext.form.field.Number', 'Ext.form.field.ComboBox', 'Ext.form.FieldSet', 'Ext.form.FieldContainer', 'Ext.form.Panel','Ext.data.Store'],
    views: [
        'Main'
    ],

    controllers: [
        'Main'
    ],

    autoCreateViewport: false
});
